package main

import "github.com/jinzhu/gorm"

// db gorm instance
var db *gorm.DB

// initial database connection
func init() {
	// Open a db connection
	var err error

	// Open database
	db, err = gorm.Open("mysql", "root:root@/todo?charset=utf8&parseTime=True&loc=Local")

	// Open database error processing
	if err != nil {
		panic(err)
	}

	// Migrate the schema(迁移模式)
	db.AutoMigrate(&TodoModel{})
}
