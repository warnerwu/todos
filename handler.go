package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// createTodo add a new todo(createTodo添加一条新的todo)
func createTodo(context *gin.Context) {
	// Get form field of the completed from post form(从post表单中获取已完成的表单字段)
	completedInt, err := strconv.Atoi(context.PostForm("completed"))

	// Transfer error process(转换错误处理)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": err,
		})
	}

	// Create todoModel struct data (创建todoModel结构数据)
	todoModel := TodoModel{Title: context.PostForm("title"), Completed: completedInt}

	// Save todoModel struct data into database(将todoModel结构数据保存到数据库中)
	db.Save(&todoModel)

	context.JSON(http.StatusCreated, gin.H{
		"status":      http.StatusCreated,
		"message":     "Todo item created successfully!",
		"resource_id": todoModel.ID,
	})
}

// fetchAllTodo fetch all todos(fetchAllTodo获取所有待办事项)
func fetchAllTodo(context *gin.Context) {
	// variable initial of the todos
	var todos []TodoModel

	// variable initial of the transformedTodo
	var transformedTodo []TransformedTodo

	// Query data for database
	db.Find(&todos)

	if len(todos) <= 0 {
		// Return data source not found
		context.JSON(http.StatusNotFound, gin.H{
			"status":  http.StatusNotFound,
			"message": "No todo found!",
		})

		// Direct return
		return
	}

	// transforms the todos for building a good response(转换todos来构建良好的响应)
	for _, todo := range todos {
		// temp variable initial of the completed(临时变量初始化完成)
		completed := false
		if todo.Completed == 1 {
			completed = true
		} else {
			completed = false
		}

		// Add the slices one by one(逐条添加到切片中)
		transformedTodo = append(transformedTodo, TransformedTodo{
			ID:        todo.ID,
			Title:     todo.Title,
			Completed: completed,
		})
	}

	// Return response data(返回响应数据)
	context.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data":   transformedTodo,
	})
}

// fetchSingleTodo fetch a single todo(fetchSingleTodo获取一个todo)
func fetchSingleTodo(context *gin.Context) {
	// variable initial of the todo
	var todo TodoModel

	// Gets the get method request parameter todoId(获取get方法请求参数todoId)
	todoId := context.Param("id")

	// First find first record that match given conditions, order by primary key
	// 首先找到匹配给定条件的第一个记录，按主键排序
	db.First(&todo, todoId)

	// Check the validity of the obtained records(对获取到的记录进行有效性检测)
	if todo.ID == 0 {
		// Return data source not found
		context.JSON(http.StatusNotFound, gin.H{
			"status":  http.StatusNotFound,
			"message": "No todo found!",
		})

		// Direct return
		return
	}

	// temp variable initial of the completed(临时变量初始化完成)
	completed := false
	if todo.Completed == 1 {
		completed = true
	} else {
		completed = false
	}

	// Return response data(返回响应数据)
	context.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data": TransformedTodo{
			ID:        todo.ID,
			Title:     todo.Title,
			Completed: completed,
		},
	})
}

// updateTodo update a todo(updateTodo更新一个todo)
func updateSingleTodo(context *gin.Context) {
	// variable initial of the todo(todo的变量初始值)
	var todo TodoModel

	// Gets the get method request parameter todoId(获取get方法请求参数todoId)
	todoId := context.Param("id")

	// First find first record that match given conditions, order by primary key
	// 首先找到匹配给定条件的第一个记录，按主键排序
	db.First(&todo, todoId)

	// Check the validity of the obtained records(对获取到的记录进行有效性检测)
	if todo.ID == 0 {
		// Return data source not found
		context.JSON(http.StatusNotFound, gin.H{
			"status":  http.StatusNotFound,
			"message": "No todo found!",
		})

		// Direct return
		return
	}

	// Update data resource of the database for title
	db.Model(&todo).Update("title", context.PostForm("title"))

	// Get form field of the completed from post form(从post表单中获取已完成的表单字段)
	completedInt, err := strconv.Atoi(context.PostForm("completed"))

	// Transfer error process(转换错误处理)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": err,
		})
	}

	// Update data resource of the database for completed
	db.Model(&todo).Update("completed", completedInt)

	// Return response data(返回响应数据)
	context.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Todo updated successfully!",
	})
}

// Delete single todo item process handler
func deleteSingleTodo(context *gin.Context) {
	// variable initial of the todo(todo的变量初始值)
	var todo TodoModel

	// Gets the get method request parameter todoId(获取get方法请求参数todoId)
	todoId := context.Param("id")

	// First find first record that match given conditions, order by primary key
	// 首先找到匹配给定条件的第一个记录，按主键排序
	db.First(&todo, todoId)

	// Check the validity of the obtained records(对获取到的记录进行有效性检测)
	if todo.ID == 0 {
		// Return data source not found
		context.JSON(http.StatusNotFound, gin.H{
			"status":  http.StatusNotFound,
			"message": "No todo found!",
		})

		// Direct return
		return
	}

	// Delete data resource of database
	db.Delete(&todo)

	// Return response data(返回响应数据)
	context.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Todo deleted successfully!",
	})
}
