package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	// Initial instance of the gin framework
	engine := gin.Default()

	v1Todos := engine.Group("/api/v1/todos")
	{
		// Create todo item
		v1Todos.POST("/", createTodo)

		// Fetch all todo
		v1Todos.GET("/", fetchAllTodo)

		// Fetch single todo
		v1Todos.GET("/:id", fetchSingleTodo)

		// Update single todo
		v1Todos.PUT("/:id", updateSingleTodo)

		// Delete single todo
		v1Todos.DELETE("/:id", deleteSingleTodo)
	}

	// Listen and server on the port 8296
	engine.Run(":8296")
}
