package main

import "github.com/jinzhu/gorm"

type (
	// todoModel describes a todoModel type(todoModel描述了todoModel类型)
	TodoModel struct {
		gorm.Model
		Title     string `json:"title"`
		Completed int    `json:"completed"`
	}

	// transformedTodo represents a formatted todo(transformedTodo表示格式化的待办事项)
	TransformedTodo struct {
		ID        uint   `json:"id"`
		Title     string `json:"title"`
		Completed bool   `json:"completed"`
	}
)
